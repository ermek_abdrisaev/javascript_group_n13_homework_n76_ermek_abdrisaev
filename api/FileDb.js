const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const filename = './messages.json';
let data = [];

module.exports = {
  async init(){
    try {
      const fileContents = await fs.readFile(filename);
      data = JSON.parse(fileContents.toString());
    } catch (e){
      data = [];
    }
  },
  getMessages(){
    return data;
  },
  addMessage(message){
    message.id = nanoid();
    data.push(message);
    return this.save();
  },
  save(){
    return fs.writeFile(filename, JSON.stringify(data, null, 2));
  }
};