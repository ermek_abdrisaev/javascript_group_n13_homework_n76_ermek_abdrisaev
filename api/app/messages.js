const express = require('express');
const db = require('../FileDb');
const router = express.Router();

router.get('/', (req, res) => {
  let date = new Date(req.query.datetime);
  if (req.query.datetime) {
    if (isNaN(date.getDate())) {
      return res.send.status(404).send({message: 'Not found'});
    } else {
      let index = db.getMessages().findIndex(message => message.datetime === req.query.datetime);
      let messages = db.getMessages().slice(index + 1, db.getMessages().length);
      console.log(messages);
      res.send(messages);
    }
    if (db.getMessages().length > 30) {
      messages = db.getMessages().slice(db.getMessages().length - 30, db.getMessages().length);
      res.send(messages);
    } else {
      messages = db.getMessages();
    }
  }

  return res.send(messages);
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.message && !req.body.message) {
      return res.status(400).send({message: "Information doesnt added! Please fill all fields..."})
    }
    const message = {
      message: req.body.message,
      author: req.body.author,
      datetime: new Date(),
    };
    await db.addMessage(message);
    res.send({message: 'Created message', id: message.id});
  } catch (e) {
    next(e);
  }
});
module.exports = router;