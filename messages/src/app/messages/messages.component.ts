import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessageService } from '../share/message.service';
import { Message } from '../share/message.model';
import { Subscriber, Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  loading = false;
  spinnerSubscription!: Subscription;
  arrMessagesSubscription!: Subscription;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.spinnerSubscription = this.messageService.spinner.subscribe( (loadingSpin: boolean) => {
      this.loading = loadingSpin;
    });
    this.arrMessagesSubscription = this.messageService.arrMessagesChange.subscribe(array => {
      this.messages = array.reverse();
    });
    this.messageService.getMessages();
  }

  ngOnDestroy() {
    this.spinnerSubscription.unsubscribe();
  }
}
