import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageService } from '../share/message.service';
import { NgForm } from '@angular/forms';
import { MessageData } from '../share/message.model';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.sass']
})
export class NewMessageComponent implements OnInit {
  @ViewChild('f') messageForm!: NgForm;
  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
  }

  onSubmit(){
    const messageData: MessageData = this.messageForm.value;
    this.messageService.createMessage(messageData).subscribe();
    this.messageForm.resetForm();
  }

}
