export class Message {
  constructor(
    public _id: string,
    public author: string,
    public message: string,
    public datetime: string,
  ){}
}

export interface MessageData {
  author: string;
  message: string;
  datetime: string;
}
