import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message, MessageData } from './message.model';
import { environment } from '../../environments/environment';
import { map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MessageService {
  private messages: Message[] = [];
  arrMessagesChange = new Subject<Message[]>();
  spinner = new Subject<boolean>();

  constructor(private http: HttpClient) {
  };

  dateTime = '';

  getMessages() {
    this.spinner.next(true);
    return setInterval(() => {
      this.http.get<Message[]>(environment.apiUrl + `/messages?datetime=${this.dateTime}`).pipe(map(response => {
          return response.map(messageData => {
            return new Message(
              messageData._id,
              messageData.author,
              messageData.message,
              messageData.datetime
            );
          });
        })
      ).subscribe(messages => {
        if (this.messages.length !== 0) {
          this.messages = this.messages.concat(messages);
          if (messages.length > 0) {
            this.dateTime = messages[messages.length - 1].datetime;
          }
          this.arrMessagesChange.next(this.messages.slice(this.messages.length - 30, this.messages.length));
          this.spinner.next(false);
        } else {
          this.messages = messages;
          this.dateTime = messages[messages.length - 1].datetime;
          this.arrMessagesChange.next(this.messages.slice());
          this.spinner.next(false);
        }

      });
    }, 3000);
  }

  createMessage(messageData: MessageData) {
    return this.http.post(environment.apiUrl + '/messages', messageData);
  }
}
